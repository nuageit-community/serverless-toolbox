FROM public.ecr.aws/docker/library/node:lts-alpine3.17

RUN apk add --no-cache python3 py3-pip \
    && npm install -g serverless

CMD [ "bin/sh" ]
