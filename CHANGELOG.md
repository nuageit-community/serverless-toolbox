# Semantic Versioning Changelog

## 1.0.0 (2023-05-15)


### :sparkles: News

* add config files and dockerfile ([76cbd79](https://gitlab.com/nuageit-community/serverless-toolbox/commit/76cbd79cb469f5304d433acc8cc8b533300e28e5))
* add gitignore, editorconfig and pipeline ([c07907d](https://gitlab.com/nuageit-community/serverless-toolbox/commit/c07907d66bb44105b0b72b9a517a7c362f092400))
* add google semantic release replace ([69b08e2](https://gitlab.com/nuageit-community/serverless-toolbox/commit/69b08e2900108783a54813a8d2c8ad1ac2194c55))
* add helm-docs ([3c2451f](https://gitlab.com/nuageit-community/serverless-toolbox/commit/3c2451fe9520dd282d833dbee0e98c34c0c681b3))
* add plugin google replace ([1a7db15](https://gitlab.com/nuageit-community/serverless-toolbox/commit/1a7db1538a9b04a911347e916cc1a3b041133f7d))
* add semantic release exec ([1e94241](https://gitlab.com/nuageit-community/serverless-toolbox/commit/1e94241326120d26179184457fd712a7bd55daa1))


### :repeat: CI

* add include job ([4f0cd6d](https://gitlab.com/nuageit-community/serverless-toolbox/commit/4f0cd6d2afc56dee9034b89fd15d515286b70aa1))
* adicionando novo template pipeline ([94ed50e](https://gitlab.com/nuageit-community/serverless-toolbox/commit/94ed50ed94f674dbb40988483b81cd0fc05d4dee))
* change include ([a1f07e3](https://gitlab.com/nuageit-community/serverless-toolbox/commit/a1f07e33846b1b2f65cd5c954715464779783766))
* correcao include ([91b393c](https://gitlab.com/nuageit-community/serverless-toolbox/commit/91b393c2f2bdf68031b6fac40e882a0b6d2ce8e0))


### :zap: Refactoring

* update file name ([9a3c41c](https://gitlab.com/nuageit-community/serverless-toolbox/commit/9a3c41cde28e4cd8024ae988456b573eee778f53))


### :bug: Fixes

* dockerfile plugin version ([31e9e5c](https://gitlab.com/nuageit-community/serverless-toolbox/commit/31e9e5c21758f29f1021a4a918b29a3194b446c8))
* dockerfile semantic release plugins versions ([2665fd2](https://gitlab.com/nuageit-community/serverless-toolbox/commit/2665fd271668c072d4fa3b0bdffd25646371bbf5))
* gitleaks config ([c97cb4b](https://gitlab.com/nuageit-community/serverless-toolbox/commit/c97cb4bb03a6ea35087d82356f8efc4d137fc3d2))
* hooks setup and makefile ([446411a](https://gitlab.com/nuageit-community/serverless-toolbox/commit/446411a828af1bc66ad8da4e67822f16ee6ca047))
* setup configurations ([9c98f94](https://gitlab.com/nuageit-community/serverless-toolbox/commit/9c98f94eaf976b9cb6c1ed9130502976b9cfa90a))


### :memo: Docs

* add content ([7378d48](https://gitlab.com/nuageit-community/serverless-toolbox/commit/7378d48957059620f337f814d3789cf71dc3eae7))
* add readme empty ([1c11311](https://gitlab.com/nuageit-community/serverless-toolbox/commit/1c11311db0733fa2cc6a1037af1cb54b229e9a05))
* empty readme ([f468539](https://gitlab.com/nuageit-community/serverless-toolbox/commit/f46853957f4fe9e0037ca1ffbbac046ade60b303))
* link git clone ([3d45285](https://gitlab.com/nuageit-community/serverless-toolbox/commit/3d4528585003650943a54420b43c52da98d9981e))
* more information ([fdd774d](https://gitlab.com/nuageit-community/serverless-toolbox/commit/fdd774d2c9213034780c5a3ec0f965b6dcf5a9bc))
* pretty content ([af513d6](https://gitlab.com/nuageit-community/serverless-toolbox/commit/af513d67be56e966434ba257d5d67aa51d6f609d))
* remove topic ([f4d134a](https://gitlab.com/nuageit-community/serverless-toolbox/commit/f4d134a67c25046b11f5ac78405e4a2938731948))
* resize image ([efd7462](https://gitlab.com/nuageit-community/serverless-toolbox/commit/efd746231d42fe9c6a13224b8907d0a356e07907))

## [1.4.0](https://gitlab.com/nuageit-community/serverless-toolbox/compare/1.3.0...1.4.0) (2023-05-15)


### :repeat: CI

* add include job ([4f0cd6d](https://gitlab.com/nuageit-community/serverless-toolbox/commit/4f0cd6d2afc56dee9034b89fd15d515286b70aa1))
* change include ([a1f07e3](https://gitlab.com/nuageit-community/serverless-toolbox/commit/a1f07e33846b1b2f65cd5c954715464779783766))


### :zap: Refactoring

* update file name ([9a3c41c](https://gitlab.com/nuageit-community/serverless-toolbox/commit/9a3c41cde28e4cd8024ae988456b573eee778f53))


### :bug: Fixes

* gitleaks config ([c97cb4b](https://gitlab.com/nuageit-community/serverless-toolbox/commit/c97cb4bb03a6ea35087d82356f8efc4d137fc3d2))
* hooks setup and makefile ([446411a](https://gitlab.com/nuageit-community/serverless-toolbox/commit/446411a828af1bc66ad8da4e67822f16ee6ca047))


### :memo: Docs

* add readme empty ([1c11311](https://gitlab.com/nuageit-community/serverless-toolbox/commit/1c11311db0733fa2cc6a1037af1cb54b229e9a05))

## [1.3.0](https://gitlab.com/nuageit-community/smr-toolbox/compare/1.2.1...1.3.0) (2023-01-18)


### :bug: Fixes

* setup configurations ([9c98f94](https://gitlab.com/nuageit-community/smr-toolbox/commit/9c98f94eaf976b9cb6c1ed9130502976b9cfa90a))


### :memo: Docs

* link git clone ([3d45285](https://gitlab.com/nuageit-community/smr-toolbox/commit/3d4528585003650943a54420b43c52da98d9981e))
* remove topic ([f4d134a](https://gitlab.com/nuageit-community/smr-toolbox/commit/f4d134a67c25046b11f5ac78405e4a2938731948))


### :sparkles: News

* add google semantic release replace ([69b08e2](https://gitlab.com/nuageit-community/smr-toolbox/commit/69b08e2900108783a54813a8d2c8ad1ac2194c55))

## [1.2.1](https://gitlab.com/nuageit-community/smr-toolbox/compare/1.2.0...1.2.1) (2023-01-17)


### :memo: Docs

* more information ([fdd774d](https://gitlab.com/nuageit-community/smr-toolbox/commit/fdd774d2c9213034780c5a3ec0f965b6dcf5a9bc))
* resize image ([efd7462](https://gitlab.com/nuageit-community/smr-toolbox/commit/efd746231d42fe9c6a13224b8907d0a356e07907))


### :repeat: CI

* adicionando novo template pipeline ([94ed50e](https://gitlab.com/nuageit-community/smr-toolbox/commit/94ed50ed94f674dbb40988483b81cd0fc05d4dee))
* correcao include ([91b393c](https://gitlab.com/nuageit-community/smr-toolbox/commit/91b393c2f2bdf68031b6fac40e882a0b6d2ce8e0))

## [1.2.0](https://gitlab.com/nuageit/devops/smr-toolbox/compare/1.1.1...1.2.0) (2022-11-27)


### :sparkles: News

* add semantic release exec ([1e94241](https://gitlab.com/nuageit/devops/smr-toolbox/commit/1e94241326120d26179184457fd712a7bd55daa1))

## [1.1.1](https://gitlab.com/nuageit/devops/smr-toolbox/compare/1.1.0...1.1.1) (2022-11-27)


### :bug: Fixes

* dockerfile plugin version ([31e9e5c](https://gitlab.com/nuageit/devops/smr-toolbox/commit/31e9e5c21758f29f1021a4a918b29a3194b446c8))

## [1.1.0](https://gitlab.com/nuageit/devops/smr-toolbox/compare/1.0.0...1.1.0) (2022-11-27)


### :sparkles: News

* add plugin google replace ([1a7db15](https://gitlab.com/nuageit/devops/smr-toolbox/commit/1a7db1538a9b04a911347e916cc1a3b041133f7d))

## 1.0.0 (2022-11-27)


### :bug: Fixes

* dockerfile semantic release plugins versions ([2665fd2](https://gitlab.com/nuageit/devops/smr-toolbox/commit/2665fd271668c072d4fa3b0bdffd25646371bbf5))


### :sparkles: News

* add config files and dockerfile ([76cbd79](https://gitlab.com/nuageit/devops/smr-toolbox/commit/76cbd79cb469f5304d433acc8cc8b533300e28e5))
* add gitignore, editorconfig and pipeline ([c07907d](https://gitlab.com/nuageit/devops/smr-toolbox/commit/c07907d66bb44105b0b72b9a517a7c362f092400))
* add helm-docs ([3c2451f](https://gitlab.com/nuageit/devops/smr-toolbox/commit/3c2451fe9520dd282d833dbee0e98c34c0c681b3))


### :memo: Docs

* add content ([7378d48](https://gitlab.com/nuageit/devops/smr-toolbox/commit/7378d48957059620f337f814d3789cf71dc3eae7))
* empty readme ([f468539](https://gitlab.com/nuageit/devops/smr-toolbox/commit/f46853957f4fe9e0037ca1ffbbac046ade60b303))
* pretty content ([af513d6](https://gitlab.com/nuageit/devops/smr-toolbox/commit/af513d67be56e966434ba257d5d67aa51d6f609d))
